import React, { Component } from "react";
import { shoeArr } from "./data_shoesShop";
import ListShoe from "./ListShoe";
import ShoesItems from "./ShoesItems";
import TableGioHang from "./TableGioHang";

export default class Ex_ShoesShopRedux extends Component {
  state = {
    shoeArr: shoeArr,
    gioHang: [],
  };

  handleAddToCart = (sp) => {
    let index = this.state.gioHang.findIndex((item) => {
      return item.id == sp.id;
    });
    let cloneGioHang = [...this.state.gioHang];
    if (index == -1) {
      let newSp = { ...sp, soLuong: 1 };
      cloneGioHang.push(newSp);
    } else {
      cloneGioHang[index].soLuong++;
    }
    this.setState({ gioHang: cloneGioHang });
  };

  handleDelete = (idshoe) => {
    let index = this.state.gioHang.findIndex((item) => {
      return item.id == idshoe;
    });
    if (index !== -1) {
      let cloneGioHang = [...this.state.gioHang];
      cloneGioHang.splice(index, 1);
      this.setState({ gioHang: cloneGioHang });
    }
  };
  handleChangeQuantity = (idshoe, step) => {
    let index = this.state.gioHang.findIndex((item) => {
      return item.id == idshoe;
    });
    let cloneGioHang = [...this.state.gioHang];
    cloneGioHang[index].soLuong = cloneGioHang[index].soLuong + step;
    if (cloneGioHang[index].soLuong == 0) {
      cloneGioHang.splice(index, 1);
    }
    this.setState({ gioHang: cloneGioHang });
  };
  render() {
    return (
      <div className="container py-5">
        {/* {this.state.gioHang.length > 0 && ( */}
        <TableGioHang
          handleDelete={this.handleDelete}
          // gioHang={this.state.gioHang}
          handleChangeQuantity={this.handleChangeQuantity}
        />
        {/* )} */}

        {/* <div className="row">{this.renderShoes()}</div> */}
        <ListShoe />
      </div>
    );
  }
}
